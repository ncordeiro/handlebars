/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.adobe.demo.handlebars.servlets;

import java.io.IOException;
import java.util.Iterator;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@SuppressWarnings("serial")
@SlingServlet(resourceTypes = "handlebars/components/structure/page", selectors = "complist", extensions = "json")
public class ComponentListServlet extends SlingSafeMethodsServlet {

    private static final Logger log = LoggerFactory.getLogger(ComponentListServlet.class);

    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
        final Resource resource = req.getResource();
        JSONObject json = new JSONObject();
        try {
            JSONArray list = new JSONArray();
            addChildren(resource, list);
            json.put("comps", list);
            resp.setContentType("application/json");
            json.write(resp.getWriter());
        } catch (JSONException ex) {
            log.error("Unable to create JSON Object", ex);
        }
    }

    private void addChildren(Resource resource, JSONArray jsonArray) {
        Iterator<Resource> children = resource.listChildren();
        while (children.hasNext()) {
            Resource child = children.next();
            try {
                JSONObject jo = new JSONObject();
                jo.put("name", child.getName());
                jo.put("resourceType", child.getResourceType());
                jo.put("path", child.getPath());
                jo.put("primaryType", child.getValueMap().get(JcrConstants.JCR_PRIMARYTYPE));
                jsonArray.put(jo);
                if (child.hasChildren()) {
                    addChildren(child, jsonArray);
                }
            } catch (JSONException ex) {
                log.error("Unable to create JSON Object", ex);
            }
        }
    }
}
