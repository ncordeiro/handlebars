(function($, templates) {

  $('.complist a').on('click', function(e) {
    e.preventDefault();
    updateComponents();
  });

  var updateComponents = function() {
    $.get(buildUrl(), replaceTable);
  }

  var replaceTable = function(data) {
    var template = templates.componentList;
    $('.complist .list-table').html(template(data));
  }

  var buildUrl = function() {
    var path = window.location.pathname.replace('.html','');
    path += '/_jcr_content.complist.json';
    return path
  }
})($, demo.templates);