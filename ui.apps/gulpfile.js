var gulp            = require('gulp'),
    concat          = require('gulp-concat'),
    declare         = require('gulp-declare'),
    handlebars      = require('gulp-handlebars'),
    wrap            = require('gulp-wrap'),
    clientlibFolder = './src/main/content/jcr_root/etc/clientlibs/handlebars/main';


gulp.task('handlebars', function() {
  gulp.src('src/**/templates/**/*.hbs')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'demo.templates',
      noRedeclare: true,
      processName: function(filePath) {
        return declare.processNameByPath(filePath.replace(/.*(\/|\\)templates(\/|\\)/, ''));//filePath.replace(/.*\/templates\//, '')
      }
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest(clientlibFolder + '/js/handlebars'));

});

gulp.task('default', ['handlebars']);